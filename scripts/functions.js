
const commons = require("./internal/commons.json");
const authentication = require('./internal/authentication');
const config = require("./internal/config");
const com = require("./internal/commons.js");
const fs = require('fs');
const path = require('path');
const { v4: uuidv4 } = require('uuid');

var lock = false;

  function recordStartTime(requestParams, context, ee, next){
    
    com.recordStartTime(context);
    return next();
  }
  
  function logger(requestParams, responseParams, context, ee, next){
    
    com.log(requestParams, responseParams, context);
    return next();
  }

  function setupConfig(context, ee, next){
    
    context.vars.global = config.config;
    context.vars.currentAntivirusLoop = 0;
    context.vars.currentBulkProcessLoop = 0;
    context.vars.currentFile = 0;
    context.vars.currentBEID = 0;
    context.vars.currentEntity = 0;
    context.vars.currentExpression = 0;
    return next();
  }

  async function cognitoAuth(context, events, done) {
    
    //No concurrent auth requests allowed
    while(lock){
      console.log("Login attempt in progress, waiting...");
      await delay(1000);
    }

    var amplifyParams = {
      region : context.vars.global.region,
      identityPoolId : context.vars.global.identityPoolId,
      userPoolId : context.vars.global.userPoolId,
      appClientId : context.vars.global.appClientId
    };

    var loginParams = {
      username : context.vars.global.username,
      password : context.vars.global.password,
      customer : context.vars.global.customer
    };

    var startAuth = Date.now();
    lock=true;
    await authentication.configure(amplifyParams);
    await authentication.handleSignIn(loginParams).then((response) => {

        var delta = Date.now() - startAuth;
        if(response.status == commons.authentication.result.ok){

          context.vars.auth = {
            customer : context.vars.global.customer,
            username: context.vars.global.username,
            session: {
              accessToken : response.user.signInUserSession.accessToken.jwtToken,
              idToken : response.user.signInUserSession.idToken.jwtToken,
              refreshToken : response.user.signInUserSession.refreshToken.token,
              expiresIn : response.user.signInUserSession.accessToken.payload.exp
            }
          };
          lock = false;
          com.writeToReportFile(`${startAuth},CognitoAuth,200,${delta}`);
        }else if(response.status == commons.authentication.result.continue){
          console.log("TODO");
          com.writeToReportFile(`${startAuth},CognitoAuth,302,${delta}`);
        }else if(response.status == commons.authentication.result.ko){
          console.log(response.error);
          com.writeToReportFile(`${startAuth},CognitoAuth,500,${delta}`);
        }  
    });
    return done();
  }

  async function cognitoPresign(context, events, done){

    var timestamp = new Date().toISOString();

    const authArgs = {
      username : context.vars.global.username,
      password : context.vars.global.password,
      validationData: {
        customer : context.vars.global.customer, 
        signature: JSON.stringify({
          credentials: context.vars.global.username,
          customer : context.vars.global.customer,
          httpMethod : "POST",
          url : context.vars.global.presignedUploadUrl,
          timestamp,
          timezone: "+0200",
          signatureReason: "bulkUpload",
          signatureComment: context.vars.fileData.comment,
        }),
      }
    };
    
    var startAuth = Date.now();

    await authentication.authForSignature(authArgs).then((response) => {
      
      var delta = Date.now() - startAuth;
      if(response.status == commons.authentication.result.ok){

        context.vars.presignData = {
            httpMethod : "POST",
            url: context.vars.global.presignedUploadUrl,
            signature: {
              accessToken: response.token,
              sessionAccessToken: context.vars.auth.session.accessToken,
            },
            timestamp,
            timezone: "+0200",
            signatureReason: "bulkUpload",
            signatureMeaning: "Automaticsignature",
            signatureComment: context.vars.fileData.comment,
            payload: {
                filename: context.vars.fileData.filename,
                contentName: context.vars.fileData.filename,
                contentType: context.vars.fileData.contentType,
                contentExtension: context.vars.fileData.contentExtension,
                uploadType: "bulkLoad",
                metadata: {
                    feature: "bulkLoad",
                    filename: context.vars.fileData.filename,
                    comment: context.vars.fileData.comment
                }
            }
        };
        com.writeToReportFile(`${startAuth},CognitoGetToken,200,${delta}`);
      }else if(response.status == commons.authentication.result.ko){
        console.log(response.error);
        com.writeToReportFile(`${startAuth},CognitoGetToken,500,${delta}`);
      }
    });
    return done();
  }

  async function getFileData(context, events, done){

    var files = fs.readdirSync(context.vars.global.filesPath);
    if(context.vars.currentFile==files.length){
      context.vars.currentFile=0;
    }
    
    var currentFile = files[context.vars.currentFile];
    com.writeToReportFile(`[INFO] Selected file ${currentFile} number ${context.vars.currentFile} of directory ${context.vars.global.filesPath}`);

    context.vars.fileData = {};
    context.vars.fileData.fullPath = path.join(context.vars.global.filesPath,currentFile);
    context.vars.fileData.filename = currentFile;
    context.vars.fileData.contentExtension = path.extname(currentFile).split('.')[1];
    if(context.vars.fileData.contentExtension== "csv"){
      context.vars.fileData.contentType = "text/csv";
    }else if(context.vars.fileData.contentExtension== "zip"){
      context.vars.fileData.contentType = "something";
    }
    context.vars.fileData.comment = "Automaticfileupload";
    context.vars.currentFile+=1;
    done();
  }

  function setFileData(requestParams, context, ee, next){
    
    requestParams.body = fs.createReadStream(context.vars.fileData.fullPath);
    return next();
  }

  function getCSRFToken(requestParams, responseParams, context, ee, next){
    
    var regexp = 'csrfToken = "(.*?)"';
    var responseBody = responseParams.body;
    context.vars.csrfToken = responseBody.match(regexp)[1];
    return next();
  }

  function getPresignedUrl(requestParams, responseParams, context, ee, next){
    
    var regexp1 = '"url":"(.*?)"';
    var regexp2 = '"key":"(.*?)"';
    var encodedURI = responseParams.body.match(regexp1)[1];
    var remoteFilename = responseParams.body.match(regexp2)[1];
    context.vars.presignedUrl = JSON.parse('"' + encodedURI + '"');
    context.vars.remoteFilename = remoteFilename;
    return next();
  }

  function getStatusUrl(context, event, done){
    
    context.vars.getStatusUrl = context.vars.target+"/data/getFileStatus?filename="+context.vars.remoteFilename+
      "&uploadType=bulkLoad&metadata="+encodeURIComponent(JSON.stringify(context.vars.presignData.payload.metadata));
    return done();
  }

  function getBulkResultUrl(context, event, done){
    
    context.vars.getBulkResultUrl = context.vars.target+"/masterdata/dataupload/getBulkResult?bulkId="+context.vars.remoteFilename;
    return done();
  }

  function generateUniqueIdentifier(context, events, done){
    
    context.vars.uuid = uuidv4();
    return done();
  }

  function statusReady(context, next) {
    
    if(!context.vars.global.waitForAntivirusToFinish){
      com.writeToReportFile("[INFO] Skipping wait for antivirus");
    }

    if(context.vars.currentAntivirusLoop == context.vars.global.maxAntivirusLoops){
      com.writeToReportFile("[INFO] Max antivirus loops reached, skipping");
      return next();
    }
    
    const continueLooping = context.vars.status == 'analysing';
    if(!continueLooping){
      if(context.vars.status == "clean"){
        com.writeToReportFile("[INFO] Virus scan ok");
      }else{
        com.writeToReportFile("[INFO] Virus scan failed");
      }
      return next();
    }
    context.vars.currentAntivirusLoop += 1;
    return next(continueLooping);
  }

  function bulkResultReady(context, next) {
    
    if(!context.vars.global.waitForBulkProcessToFinish){
      com.writeToReportFile("[INFO] Skipping wait for bulk process import");
    }

    if(context.vars.currentBulkProcessLoop == context.vars.global.maxBulkProcessLoops){
      com.writeToReportFile("[INFO] Max bulk upload loops reached, skipping");
      return next();
    }

    const continueLooping = context.vars.result == 'false';
    if(!continueLooping){
      var actualResult = JSON.parse(context.vars.result);

      if(!actualResult.files[0].result){
        com.writeToReportFile(`[ERROR] File processing failed with reason ${actualResult.files[0].error.code} and message ${actualResult.files[0].error.message}`);
      }else{
        if(actualResult.files[0].result.status == 'PartialInserted'){
          com.writeToReportFile(`[WARNING] File processed with some values missing, inserted values ${actualResult.files[0].result.validValues},
            non inserted value ${actualResult.files[0].result.nonInserted}`);
        }else if(actualResult.files[0].result.status == 'Success'){
          com.writeToReportFile("[INFO] File processing ok, all values inserted");
        }else{
          com.writeToReportFile(`[INFO] Unrecognized file status: ${actualResult}`);
        }
      }
      return next();
    }
    context.vars.currentBulkProcessLoop += 1;
    return next(continueLooping);
  }

  function getEpochDate(context, event, done){
    context.vars.currentEpoch =  Math.round(Date.now() / 1000);
    return done();
  }

  function getISODate(context, event, done){

    context.vars.isoDate = new Date().toISOString();
    return done();
  }

  function getFirstBEID(context, event, done){
    context.vars.targetBEID = context.vars.global.getAfterValuesBEID[context.vars.currentBEID];
    context.vars.currentBEID+=1;
    return done();
  }

  function iterateThroughBEIDs(context, next){

    if(context.vars.currentBEID == context.vars.global.getAfterValuesBEID.length){
      return next();
    }else{
      context.vars.targetBEID = context.vars.global.getAfterValuesBEID[context.vars.currentBEID];
      context.vars.currentBEID+=1;
      return next(true);
    }

  }

  function getFirstEntity(context, event, done){
    context.vars.targetEntity = context.vars.global.getLastValueEntities[context.vars.currentEntity];
    context.vars.currentEntity+=1;
    return done();
  }

  function iterateThroughEntities(context, next){

    if(context.vars.currentEntity == context.vars.global.getLastValueEntities.length){
      return next();
    }else{
      context.vars.targetEntity = context.vars.global.getLastValueEntities[context.vars.currentEntity];
      context.vars.currentEntity+=1;
      return next(true);
    }

  }

  function getFirstExpression(context, event, done){
    context.vars.targetExpression = context.vars.global.expressions[context.vars.currentExpression];
    context.vars.currentExpression+=1;
    return done();
  }

  function iterateThroughExpressions(context, next){

    if(context.vars.currentExpression == context.vars.global.expressions.length){
      return next();
    }else{
      context.vars.targetExpression = context.vars.global.expressions[context.vars.currentExpression];
      context.vars.currentExpression+=1;
      return next(true);
    }

  }

  const delay = ms => new Promise(res => setTimeout(res, ms));

  module.exports = {
    cognitoAuth : cognitoAuth,
    cognitoPresign : cognitoPresign,
    getCSRFToken : getCSRFToken,
    getPresignedUrl : getPresignedUrl,
    setupConfig : setupConfig,
    logger : logger,
    recordStartTime : recordStartTime,
    generateUniqueIdentifier : generateUniqueIdentifier,
    getFileData : getFileData,
    setFileData : setFileData,
    getStatusUrl : getStatusUrl,
    getBulkResultUrl : getBulkResultUrl,
    statusReady : statusReady,
    bulkResultReady : bulkResultReady,
    getEpochDate : getEpochDate,
    getISODate : getISODate,
    iterateThroughBEIDs : iterateThroughBEIDs,
    getFirstBEID : getFirstBEID,
    getFirstEntity : getFirstEntity,
    iterateThroughEntities : iterateThroughEntities,
    getFirstExpression : getFirstExpression,
    iterateThroughExpressions : iterateThroughExpressions
  };