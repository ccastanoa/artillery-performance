
const fs = require('fs');
const commons = require("./commons.json");

function log(requestParams, responseParams, context){
    //console.log(responseParams);
    if ( context.vars._startTime) {
        var delta = Date.now() - context.vars._startTime;
        var output = context.vars._startTime+","+requestParams.method+","+responseParams.requestUrl+","+responseParams.statusCode+","+delta;
        if(requestParams.method=="POST" | requestParams.method=="DELETE"){
            output+= ","+JSON.stringify(requestParams.json);
        }else if(requestParams.method=="PUT"){
            output+= ","+JSON.stringify(requestParams.body);
        }
        writeToReportFile(output);
    }
}

function recordStartTime(context) {
    context.vars._startTime = Date.now();
}

function writeToReportFile(trace){
    fs.appendFileSync(commons.output.paths.responses+commons.output.paths.responsesExtension, trace+"\r\n");
}

module.exports = {
    log : log,
    recordStartTime : recordStartTime,
    writeToReportFile : writeToReportFile,
  };
