const YAML = require('yaml');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');

class Config {

    constructor() {
        var allConfig;
        var internalConfigFiles = traverseDir(path.resolve(__dirname, '../../'),[]);
        internalConfigFiles.forEach(function(element){
            var currentConfigFile = fs.readFileSync(element, 'utf8');
            allConfig= _.merge(YAML.parse(currentConfigFile),allConfig);
        });
        this.config = allConfig;
    }

    get() {
        return this.config;
    }
}

module.exports = new Config();

function traverseDir(dir,fileList) {
    fs.readdirSync(dir).forEach(file => {
      let fullPath = path.join(dir, file);
      if (fs.lstatSync(fullPath).isDirectory()) {
         traverseDir(fullPath,fileList);
       } else {
         if(path.extname(fullPath)==".yml")
            fileList.push(fullPath);
       }  
    });
    return fileList;
}