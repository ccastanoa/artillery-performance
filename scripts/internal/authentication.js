const Auth = require('@aws-amplify/auth');
const commons = require("./commons.json");

module.exports = {
  handleSignIn : handleSignIn,
  configure: configure,
  authForSignature: authForSignature
}

async function configure({region, identityPoolId, userPoolId, appClientId}){

  return Auth.Auth.configure({
    region,
    identityPoolId,
    userPoolId,
    userPoolWebClientId: appClientId
  });

};

async function handleSignIn ({ username, password, customer }){
   
    try {
      const session = await Auth.Auth.signIn({
        username,
        password,
        validationData: { customer }
      });

      if (
        session.authenticationFlowType === 'USER_SRP_AUTH' &&
        session.challengeName === 'SOFTWARE_TOKEN_MFA'
      ) {
        return ({ status: commons.authentication.result.continue, user: session });
      } else {
        return ({ status: commons.authentication.result.ok, user: session});
      }
    } catch (e) {
      return ({ status: commons.authentication.result.ko, error: e });
    }
};

async function authForSignature(authArgs){

  try {
    const signUser = await Auth.Auth.signIn(authArgs);
    if (signUser.challengeName === 'SOFTWARE_TOKEN_MFA') {
      return ({ status: commons.authentication.result.continue, token: signUser });
    }
    return ({ status: commons.authentication.result.ok, token: signUser.signInUserSession.accessToken.jwtToken });
  } catch (err) {
    return ({ status: commons.authentication.result.ko, error: e });
  }

};
