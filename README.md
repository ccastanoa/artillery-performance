# artillery-performance

This repository contains [artillery.io](https://artillery.io/) scripts for load testing of the Aizon platform UI. artillery.io is a load testing solution intended to test the performance of a software web component through mimicking browser actions. For further information of Artillery syntax check [documentation](https://artillery.io/docs/guides/overview/welcome.html)

## 🗂 Table of contents
- [Pre-requirements](##Pre-requirements)
- [Software requirements](##Software-requirements)
- [Folder structure](##Folder-structure)
- [Installation](##Installation)
- [Configuration](##Configuration)
- [Execution](##Execution)
- [Reporting](##Reporting)

## ✔️ Pre-requirements

In order to execute the tests the following information is required:

- URL of the target Aizon platform instance UI.
- Valid credentials to access the Aizon platform instance.
- Customer code of the Aizon platform instance.
- AWS region of the Aizon platform instance.
- IdentityPoolId of the AWS Cognito Identity pool where the credentials are contained (can be retrieved throuhg AWS console).
- userPoolId of the AWS Cognito Identity pool where the credentials are contained (can be retrieved throuhg AWS console).
- appClientId of the Aizon platform instance in AWS (can be retrieved throuhg AWS console)
- url of the lambda data upload endpoint for file upload

## ⚙️ Software-requirements

Artillery scripts run over Node so a NodeJS + NPM installation is required (v>= 12)

## 📂 Folder structure

Repository has the following folder structure

```bash
root/
├── scenarios/ #Directory containing the test scenarios in YAML format
└── scripts/ #Directory containing the processors for scenarios
│   ├── internal/ #Directory containing JS scripts used by processors 
├── config.yml #YAML configuration file
```

## 🏗 Installation

1. Clone this repo
2. Install the dependencies of the test scripts by running the following command from root folder
```bash
npm install
```

## ✍🏼 Configuration

File [config.yml](./config.yml) contains the configuration keys that must be set in order to execute the tests. Keys are

```yaml
username: #Aizon platform username
password: #Username's password
customer: #Customer code (qa, dev, sandbox...)
region: #AWS region
identityPoolId: #Cognito Identity pool id
userPoolId: #Cognito user pool id
appClientId: #AWS app client id
presignedUploadUrl: #Lambda URL of the presigned upload for bulk upload (only required if bulk upload test is going to be executed)
filesPath: #Directory path of the files to be uploaded (only required if bulk upload tests is going to be executed)
maxAntivirusLoops:  #Maximum number of request to launch to antivirus endpoints before skipping (only required if bulk upload tests is going to be executed)
maxBulkProcessLoops: #Maximum number of request to launch to bulk upload status endpoints before skipping (only required if bulk upload tests is going to be executed)
waitForAntivirusToFinish: #If true each virtual user will wait for antivirus to end before continuing (only required if bulk upload tests is going to be executed)
waitForBulkProcessToFinish: #If true each virtual user will wait for bulk process to end before continuing (only required if bulk upload tests is going to be executed)
userAgent: #User agent for solutions test request (only required if solutions test is going to be executed)
solutionName: #Name of the solution (only required if solutions test is going to be executed)
getAfterValuesBEID: #JSON array containing the requests to /getAfterValues endpoint to be executed by a virtual user (JSON fields beid, startDate) (only required if solutions test is going to be executed)
getLastValueEntities: #JSON array containing the requests to /getLastEntitiesValues endpoint to be executed by a virtual user (JSON fields entityName, entityType) (only required if solutions test is going to be executed)
expressions: # JSON array containing data for expressions for request to /evaluateExpression endpoint to be executed by a virtual user (JSON fields entity, startDate, endDate) (only get values in timeframe currently supported as valid expression) (only required if solutions test is going to be executed)
```

## 👩🏻‍💻 Execution

🛑🛑🛑 **CHECK [SCENARIOS README](./scenarios/README.md) FOR ESSENTIAL INFO BEFORE EXECUTION** 🛑🛑🛑

The following test scenarios are currently available:

* login
* listBEDF
* listDevices
* listElements
* listValueAssociations
* bulkUpload
* solutions

To execute them run the following from the root folder:

* `npm run login`
* `npm run listBEDF`
* `npm run listElements`
* `npm run listDevices`
* `npm run listValueAssociations`
* `npm run bulkUpload`
* `npm run solutions`

Scenarios contain important configuration keys. For a detailed explanation of the existing test scenarios AND HOW TO CONFIGURE THEM please check [secenarios readme](./scenarios/README.md)

## 📒 Reporting

Each test run will generate a CSV file "result.csv" containing the following information per request.

`UnixTimestamp,HTTPMethod,URL,HTTPResponseStatusCode,RTT`

If the requests is of type POST, DELETE or PUT it will also log the payload
