# artillery-performance test scenarios

This folder contains the currently existing load test scenarios (as YAML Artilery scripts) for the Aizon platform. This scenarios contain important configuration keys that must be reviewed before execution.

These scenarios mimick the behavior of a web browser. This is, they reproduce all the request that a web browser does when executing a given workflow (downloading all resources). These scenarios also mimick the prallelization of requests done by the browsers.

## Table of contents
- [Scenarios](##Scenarios)
    - [Login](###Login)
    - [List BEDF](###List-BEDF)
    - [List Elements](###List-Elements)
    - [List Devices](###List-Devices)
    - [List Value Associations](###List-Value-Associations)
    - [Bulk upload](###Bulk-upload)
- [How to create new scenarios](##How-to-create-new-scenarios)

## Scenarios

Implemented scenarios

### Login

This scenario reproduces the behavior of a user that acesses the home page, performs a successful login and lands in the home page.

#### Configuaration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot

### List BEDF

This scenario reproduces the behavior of user that logs in (without downloading additional resources) and acesses the BEDF list page (downloading additional resources)

#### Configuration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot
* **loop count**: How many times is the logged in virtual user going to request the BEDF page (located in the bottom of the script)

### List Elements

This scenario reproduces the behavior of user that logs in (without downloading additional resources), acesses the Elements list page (downloading additional resources) and navigates through the different Elements list pages

#### Configuration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot
* **loop count**: Elements pages the logged in virtual user is going to visit (with query params) (located in the bottom of the script)

### List Devices

This scenario reproduces the behavior of user that logs in (without downloading additional resources), acesses the Devices list page (downloading additional resources) and navigates through the different Device list pages

#### Configuration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot
* **loop count**: Devices pages the logged in virtual user is going to visit (with query params) (located in the bottom of the script)

### List Value Associations

This scenario reproduces the behavior of user that logs in (without downloading additional resources), acesses the Value Associations list page (downloading additional resources) and navigates through the different Value Associations list pages

#### Configuration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot
* **loop** count: Value Association pages the logged in virtual user is going to visit (with query params) (located in the bottom of the script)

### Bulk upload

This scenario reproduces the behavior of user that logs in (without downloading additional resources), acesses the Bulk load page (downloading additional resources) and uploads a file or a list of files

#### Configuration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot
* **filesPerVirtualUser**: Number of files of the files directory set in config file each virtual user is going to upload. Files are uploaded one per one and each virtual user will itearte the folder getting one file at a time. If value is set to 2 and directory contains 3 files each virtual user will upload the first 2 (in alphabetical order)
* **secondsBetweenAntivirusQueries**: Seconds each virtual user sleeps between each request to the antivirus status endpoint. Only applies if config key waitForAntivirusToFinish is set to true.
* **secondsBetweenBulkStatusQueries**: Seconds each virtual user sleeps between each request to the bulk import status endpoint. Only applies if config key waitForBulkProcessToFinish is set to true.

### Solutions

This scenario reproduces the behavior of user that logs in (without downloading additional resources), and acesses as solution. This scenario does not reproduce the exact behavior of any solution but reproduces the high-cost operations done by a certain solution

#### Configuration params

* **target**: Aizon platform home URL
* **duration**: Time slot in which all virtual users are going to be launched
* **arrivalCount**: Total number of virtual users to launch in the time slot

## How to create new scenarios

Some tips to create new scenarios:

**Check the docs**: Artillery configuration and syntax is not obvious. Before trying to create a new scenario check the docs carefully.

**Auto-generate the baic structure of the scenario**: To generate the basic structure of the artillyer scenario execute the flow to be mimicked through a web browser with dev tools enabled, then download the captured requests as a HAR file and use this tool [artillery-har-to-yaml](https://github.com/charlesmst/artillery-har-to-yaml) to generate an Artillery YAML scenario. Have in mind that the scenario "as generated" is not usable as it has everything hardcoded.

**Import the configuration**: Artillery does not offer natively a configuration loader so configuration variables need to be imported manually. To do so include the following as the first line of an scenario.

```yaml
- function: "setupConfig"
```

**Include logger in each request**: Artillery does not offer natively a logger for each trace so response times and statuses need to be logged manually. To do so include the following in each request.

```yaml
beforeRequest: "recordStartTime"
afterResponse: "logger"
```

**Use parallel requests**: Browsers execute some requests in parallel but Artillery, by default, executes them sequentially. To reproduce the exact behavior of a user in a browser and avoid test execution taking forever use undocumented Artillery feature "parallel". You wouldn't find it in the docs but it is working fine as in version 1.7. It is used as this.

```yaml
- get: /something
- parallel:
    - get: /js/someScript.js
    - get: /js/otherScript.js
    - get: /css/someStyleSheet.js
- get: /somethingElse
```

**Update the readmes and the scripts section**: Once the new scenario has been added please update this readme, the main readme and the scripts section of the package.json file.